﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Warehouse.WpfClient.WarehouseApi;
using Warehouse.WpfClient.WarehouseApi.Models;

namespace Warehouse.WpfClient.Components
{
    /// <summary>
    /// Interaction logic for DestrictDetailsPageView.xaml
    /// </summary>
    public partial class DestrictDetailsPageView : Page
    {
        public DestrictDetailsPageView()
        {
            InitializeComponent();
        }
       
        /// <summary>
        /// Action handler for creating VM and passing the relevant District ID to it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DistrictDetailsPageView_OnLoaded(object sender, RoutedEventArgs e)
        {
            var districtId = Application.Current.Properties[Consts.pDISTRICTS_TO_DISTRICTDETAILS];
            DataContext = new DistrictDetailsPageViewModel((string)districtId);
        }

        /// <summary>
        /// Action handler for updating of existing sales person from the currently loaded district.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            SalesPerson salesPerson = (sender as DataGridRow)?.DataContext as SalesPerson;
            Application.Current.Properties[Consts.pDISTRICTDETAILS_TO_ADDUPDATESALESPERSON_districtId] = ((DistrictDetailsPageViewModel)DataContext).DistrictId; 
            Application.Current.Properties[Consts.pDISTRICTDETAILS_TO_ADDUPDATESALESPERSON_salesPerson] = salesPerson;
            NavigationService?.Navigate(new AddUpdateSalesPersonPageView());
        }

        /// <summary>
        /// Action handler for creating a new sales person in the currently loaded district.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewSalesPersonButton_OnClick(object sender, RoutedEventArgs e)
        {
            Application.Current.Properties[Consts.pDISTRICTDETAILS_TO_ADDUPDATESALESPERSON_districtId] = ((DistrictDetailsPageViewModel)DataContext).DistrictId;
            Application.Current.Properties[Consts.pDISTRICTDETAILS_TO_ADDUPDATESALESPERSON_salesPerson] = null;
            NavigationService?.Navigate(new AddUpdateSalesPersonPageView());
        }
    }
}