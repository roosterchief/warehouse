using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Windows;
using Warehouse.WpfClient.WarehouseApi;
using Warehouse.WpfClient.WarehouseApi.Models;

namespace Warehouse.WpfClient.Components
{
    public class AddUpdateSalesPersonPageViewModel
    {
        public AddUpdateSalesPersonPageViewModel(string districtId, SalesPerson salesPerson)
        {
            if (string.IsNullOrEmpty(salesPerson?.ID)) // Add
            {
                Title = "Add Salesperson";

                try
                {
                    var client = new WarehouseApiClient(new Uri(ConfigurationManager.AppSettings["WarehouseApiUrl"]));
                    List<SalesPerson> allSalesPersons = client.SalesPersons.GetAll().ToList(); //  Equal to the API call: '/api/salespersons'

                    allSalesPersons.Insert(0, new SalesPerson { ID = "", FirstName = "<New>", Initials = "", LastName = "" });
                    SalesPersons = allSalesPersons.ToList();
                    SelectedSalesPerson = SalesPersons[0];
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    MessageBox.Show("Something went wrong.");
                }
                
                IsPrimary = false;
                IsSecondary = false;
            }
            else // Update
            {
                SalesPersons = new List<SalesPerson> { new SalesPerson { ID = salesPerson.ID, FirstName = salesPerson.FirstName, LastName = salesPerson.LastName, Initials = salesPerson.Initials} };
                Title = $"Update {salesPerson.FirstName} {salesPerson.LastName}";

                try
                {
                    var client = new WarehouseApiClient(new Uri(ConfigurationManager.AppSettings["WarehouseApiUrl"]));
                    DistrictResponse district = client.Districts.Get(districtId); //  Equal to the API call: '/api/districts/{districtId}'

                    IsPrimary = district.PrimarySalesPerson != null && district.PrimarySalesPerson.ID == salesPerson.ID;
                    IsSecondary = district.SecondarySalesPersons.FirstOrDefault(x => x.ID == salesPerson.ID) != null ;
                    SelectedSalesPerson = SalesPersons[0];
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    MessageBox.Show("Something went wrong.");
                }
            }

            DistrictId = districtId;
            
        }

        public List<SalesPerson> SalesPersons { get; set; }
        public string Title { get; set; }
        public string DistrictId { get; set; }
        public SalesPerson SelectedSalesPerson { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsSecondary { get; set; }
    }
}