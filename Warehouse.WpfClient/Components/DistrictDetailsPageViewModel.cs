﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Windows;
using Warehouse.WpfClient.WarehouseApi;
using Warehouse.WpfClient.WarehouseApi.Models;


namespace Warehouse.WpfClient.Components
{
    public class DistrictDetailsPageViewModel
    {
        public DistrictDetailsPageViewModel(string districtId)
        {
            try
            {
                var client = new WarehouseApiClient(new Uri(ConfigurationManager.AppSettings["WarehouseApiUrl"]));
                DistrictResponse response = client.Districts.Get(districtId); // Equal to the API call: '/api/districts/{districtId}'

                PrimarySalesPerson = response.PrimarySalesPerson != null
                    ? new List<SalesPerson> { response.PrimarySalesPerson }
                    : new List<SalesPerson> { new SalesPerson { FirstName = "", LastName = "", ID = "", Initials = "" } };
                SecondarySalesPersons = response.SecondarySalesPersons.ToList();
                Stores = response.Stores.ToList();
                Name = response.Name;
                DistrictId = response.DistrictId;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show("Something went wrong.");
            }
        }

        public List<SalesPerson> PrimarySalesPerson { get; set; } // NOTE: This list has only one record all the time! Because we load the primary saLES person in grid
        public List<SalesPerson> SecondarySalesPersons { get; set; }
        public List<Store> Stores { get; set; }
        public string Name { get; set; }
        public string DistrictId { get; internal set; }
    }
}
