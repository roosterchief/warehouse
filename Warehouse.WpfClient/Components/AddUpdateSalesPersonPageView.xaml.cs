﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Rest;
using Warehouse.WpfClient.WarehouseApi;
using Warehouse.WpfClient.WarehouseApi.Models;

namespace Warehouse.WpfClient.Components
{
    /// <summary>
    /// Interaction logic for AddUpdateSalesPersonPageView.xaml
    /// </summary>
    public partial class AddUpdateSalesPersonPageView : Page
    {
        public AddUpdateSalesPersonPageView()
        {
            InitializeComponent();
                
            DataContext = new AddUpdateSalesPersonPageViewModel((string)Application.Current.Properties[Consts.pDISTRICTDETAILS_TO_ADDUPDATESALESPERSON_districtId],
                                                                    (SalesPerson)Application.Current.Properties[Consts.pDISTRICTDETAILS_TO_ADDUPDATESALESPERSON_salesPerson]);         
        }
       
        /// <summary>
        /// Action hander for saving chnages of new/updated sales person.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveSalesPersonButton_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var viewModel = (AddUpdateSalesPersonPageViewModel)DataContext;

                var client = new WarehouseApiClient();
                client.SalesPersons.AddUpdate(viewModel.SelectedSalesPerson.ID, 
                                                new SalesPersonAddUpdateRequest {DistrictId = viewModel.DistrictId, IsPrimary = viewModel.IsPrimary, IsSecondary = viewModel.IsSecondary});                
            }
            catch(HttpOperationException ex)
            {
                if (ex.Response.StatusCode == HttpStatusCode.BadRequest)
                {
                    MessageBox.Show(ex.Response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception ex) 
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show("Something went wrong.");
            }

            CleanAndMoveBack();
        }

        private void Hyperlink_OnClick(object sender, RoutedEventArgs e)
        {
            CleanAndMoveBack();
        }

        #region Private Methods
        private void CleanAndMoveBack()
        {
            Application.Current.Properties[Consts.pDISTRICTDETAILS_TO_ADDUPDATESALESPERSON_districtId] = null;
            Application.Current.Properties[Consts.pDISTRICTDETAILS_TO_ADDUPDATESALESPERSON_salesPerson] = null;
            NavigationService?.Navigate(new DestrictDetailsPageView());
        }
        #endregion
    }
}
