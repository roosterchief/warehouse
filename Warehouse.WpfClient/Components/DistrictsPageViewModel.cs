﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Windows;
using Warehouse.WpfClient.WarehouseApi;
using Warehouse.WpfClient.WarehouseApi.Models;

namespace Warehouse.WpfClient.Components
{
    public class DistrictsPageViewModel
    {
        public DistrictsPageViewModel()
        {
            try
            {
                var client = new WarehouseApiClient(new Uri(ConfigurationManager.AppSettings["WarehouseApiUrl"]));
                DistrictListResponse results = client.Districts.GetAll(); // Equal to the API call: '/api/districts'
                Districts = results.Districts.ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show("Something went wrong.");
            }
        }
        
        public List<District> Districts { get; set; }
    }
}
