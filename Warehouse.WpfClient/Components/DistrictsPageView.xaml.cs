﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Warehouse.WpfClient.WarehouseApi.Models;

namespace Warehouse.WpfClient.Components
{
    /// <summary>
    /// Interaction logic for DistrictsPageView.xaml
    /// </summary>
    public partial class DistrictsPageView : Page
    {
        public DistrictsPageView()
        {
            InitializeComponent();
        }

        private void DistrictsPageView_OnLoaded(object sender, RoutedEventArgs e)
        {   
            DataContext = new DistrictsPageViewModel();
        }

        /// <summary>
        /// Action handler for navigating to the details of the currently selected district.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataGridDistricts_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            District district = (sender as DataGridRow)?.DataContext as District;
            Application.Current.Properties[Consts.pDISTRICTS_TO_DISTRICTDETAILS] = district.ID;
            NavigationService?.Navigate(new DestrictDetailsPageView());            
        }
    }
}
