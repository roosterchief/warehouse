﻿using System;
using System.Collections.Generic;
using Warehouse.DataAccess.Entities;

namespace Warehouse.DataAccess
{
    public interface IStoreRepository
    {
        List<Store> GetStoresByDistrictId(Guid id);
    }
}
