﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using Warehouse.DataAccess.Entities;

namespace Warehouse.DataAccess
{
    public class StoreRepository : RepositoryBase, IStoreRepository
    {
        public List<Store> GetStoresByDistrictId(Guid id)
        {
            return Db.Query<Store>("SELECT Id, Name, Address FROM Stores WHERE DistrictId = @DistrictId", new { DistrictId = id }).ToList();
        }
    }
}
