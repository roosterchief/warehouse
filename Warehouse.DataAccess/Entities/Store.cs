﻿using System;

namespace Warehouse.DataAccess.Entities
{
    public class Store
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }
    }
}
