﻿using System;

namespace Warehouse.DataAccess.Entities
{
    public class District
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
