﻿using System;

namespace Warehouse.DataAccess.Entities
{
    public class SalesPerson
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Initials { get; set; }
    }
}
