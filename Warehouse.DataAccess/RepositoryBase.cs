﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Warehouse.DataAccess
{
    public abstract class RepositoryBase : IDisposable
    {
        protected readonly IDbConnection Db = new SqlConnection(ConfigurationManager.ConnectionStrings["warehouseDB"].ConnectionString);

        public void Dispose()
        {
            Db?.Dispose();
        }

    }
}
