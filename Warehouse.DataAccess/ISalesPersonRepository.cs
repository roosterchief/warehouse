﻿using System;
using System.Collections.Generic;
using Warehouse.DataAccess.Entities;

namespace Warehouse.DataAccess
{
    public interface ISalesPersonRepository
    {
        SalesPerson GetSalesPerson(Guid id);

        SalesPerson GetPrimarySalesPersonByDistrictId(Guid id);

        List<SalesPerson> GetSecondarySalesPersonsByDistrictId(Guid id);

        List<SalesPerson> GetAll();

        void AddUpdateSalesPersonDistrictRelation(Guid id, Guid districtId, bool isPrimary, bool isSecondary);

        void RemoveSalesPersonDistrictRelation(Guid id, Guid districtId);
    }
}
