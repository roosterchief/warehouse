﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using Warehouse.DataAccess.Entities;

namespace Warehouse.DataAccess
{
    public class DistrictRepository : RepositoryBase, IDistrictRepository
    {
        public List<District> GetAllDistricts()
        {
            return Db.Query<District>("SELECT * FROM Districts ORDER BY Name").ToList();
        }

        public District GetDistrict(Guid id)
        {
            return Db.Query<District>("SELECT * FROM Districts WHERE Id = @DistrictId", new { DistrictId = id }).FirstOrDefault();
        }
    }
}
