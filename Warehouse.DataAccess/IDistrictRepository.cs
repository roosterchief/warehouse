﻿using System;
using System.Collections.Generic;
using Warehouse.DataAccess.Entities;

namespace Warehouse.DataAccess
{
    public interface IDistrictRepository
    {
        List<District> GetAllDistricts();

        District GetDistrict(Guid id);
    }
}
