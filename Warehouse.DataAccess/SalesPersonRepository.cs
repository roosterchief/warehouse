﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Warehouse.DataAccess.Entities;

namespace Warehouse.DataAccess
{
    public class SalesPersonRepository : RepositoryBase, ISalesPersonRepository
    {
        public SalesPerson GetSalesPerson(Guid id)
        {
            return Db.Query<SalesPerson>("SELECT sp.Id, sp.FirstName, sp.LastName, sp.Initials FROM SalesPersons sp " +
                                         "WHERE sp.Id = @SalesPersonId", new { SalesPersonId = id }).SingleOrDefault();
        }

        public SalesPerson GetPrimarySalesPersonByDistrictId(Guid id)
        {
            return Db.Query<SalesPerson>("SELECT sp.Id, sp.FirstName, sp.LastName, sp.Initials " +
                                             "FROM SalesPersons sp " +
                                             "JOIN DistrictsSalesPersons dsp " +
                                             "ON sp.Id = dsp.SalesPersonId " +
                                             "WHERE dsp.DistrictId = @DistrictId AND dsp.IsPrimary = 1", new { DistrictId = id }).SingleOrDefault();

        }

        public List<SalesPerson> GetSecondarySalesPersonsByDistrictId(Guid id)
        {
            return Db.Query<SalesPerson>("SELECT sp.Id, sp.FirstName, sp.LastName, sp.Initials " +
                                             "FROM SalesPersons sp " +
                                             "JOIN DistrictsSalesPersons dsp " +
                                             "ON sp.Id = dsp.SalesPersonId " +
                                             "WHERE dsp.DistrictId = @DistrictId AND dsp.IsSecondary = 1", new { DistrictId = id }).ToList();
        }

        public List<SalesPerson> GetAll()
        {
            return Db.Query<SalesPerson>("SELECT sp.Id, sp.FirstName, sp.LastName, sp.Initials FROM SalesPersons sp").ToList();
        }

        public void AddUpdateSalesPersonDistrictRelation(Guid id, Guid districtId, bool isPrimary, bool isSecondary)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@DistrictId", districtId);
            parameters.Add("@SalesPersonId", id);
            parameters.Add("@IsPrimary", Convert.ToInt16(isPrimary));
            parameters.Add("@IsSecondary", Convert.ToInt16(isSecondary));

            Db.Execute("dbo.AddUpdateSalesPersonDistrictRelation", parameters, commandType: CommandType.StoredProcedure);
        }

        public void RemoveSalesPersonDistrictRelation(Guid id, Guid districtId)
        {
            Db.Execute("DELETE FROM DistrictsSalesPersons WHERE SalesPersonId = @Id AND DistrictId = @DistrictId", new { Id = id, DistrictId = districtId });
        }
    }
}
