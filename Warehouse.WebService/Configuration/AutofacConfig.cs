﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Owin;
using Warehouse.DataAccess;
using Warehouse.WebService.Controllers;
using Warehouse.WebService.Filters;
using Warehouse.WebService.Middleware;
using Warehouse.WebService.Services;

namespace Warehouse.WebService.Configuration
{
    public class AutofacConfig
    {
        public static void Setup(IAppBuilder appBuilder, HttpConfiguration config)
        {
            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterWebApiFilterProvider(config);

            // Filters
            builder.RegisterType<LogExceptionFilter>().AsWebApiExceptionFilterFor<ApiControllerBase>().InstancePerRequest();

            // Services
            builder.RegisterType<Logger>().As<ILogger>().InstancePerRequest(); 
            builder.RegisterType<DistrictsService>().As<IDistrictsService>().InstancePerRequest();
            builder.RegisterType<SalesPersonsService>().As<ISalesPersonsService>().InstancePerRequest();
            
            // Repositories
            builder.RegisterType<DistrictRepository>().As<IDistrictRepository>();
            builder.RegisterType<SalesPersonRepository>().As<ISalesPersonRepository>();
            builder.RegisterType<StoreRepository>().As<IStoreRepository>();

            // Middleware
            builder.RegisterType<RequestLoggingMiddleware>().InstancePerRequest();
            
            IContainer container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            appBuilder.UseAutofacMiddleware(container);
            appBuilder.UseAutofacWebApi(config);
        }
    }
}
