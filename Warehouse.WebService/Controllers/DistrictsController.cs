﻿using System;
using System.Web.Http;
using System.Web.Http.Description;
using Warehouse.WebService.Exceptions;
using Warehouse.WebService.Models;
using Warehouse.WebService.Services;

namespace Warehouse.WebService.Controllers
{
    [RoutePrefix("api/districts")]
    public class DistrictsController : ApiControllerBase
    {
        private readonly IDistrictsService _districtsService;
        
        public DistrictsController(IDistrictsService districtsService, ILogger logger) : base(logger)
        {
            _districtsService = districtsService;
        }

        /// <summary>
        /// Gets all districts.
        /// </summary>
        /// <returns></returns>
        [Route("")]
        [HttpGet]
        [ResponseType(typeof(DistrictListResponse))]
        public IHttpActionResult GetAll()
        {
            DistrictListResponse responseModel = _districtsService.GetAll();

            return Ok(responseModel);            
        }

        /// <summary>
        /// Gets single district.
        /// </summary>
        /// <param name="districtId"></param>
        /// <returns></returns>
        [Route("{districtId:guid}")]
        [HttpGet]
        [ResponseType(typeof(DistrictResponse))]
        public IHttpActionResult Get(Guid districtId)
        {
            DistrictResponse response;
            try
            {
                response = _districtsService.Get(districtId);
            }
            catch (NotFoundDistrictException)
            {
                return NotFound();
            }

            return Ok(response);
        }
    }
}
