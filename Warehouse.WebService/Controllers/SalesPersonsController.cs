﻿using System;
using System.Web.Http;
using System.Web.Http.Description;
using Warehouse.BusinessLogic.Models;
using Warehouse.WebService.Exceptions;
using Warehouse.WebService.Models;
using Warehouse.WebService.Services;

namespace Warehouse.WebService.Controllers
{
    [RoutePrefix("api/salespersons")]
    public class SalesPersonsController : ApiControllerBase
    {
        private readonly ISalesPersonsService _salesPersonsService;
        
        public SalesPersonsController(ISalesPersonsService salesPersonsService, ILogger logger) : base(logger)
        {
            _salesPersonsService = salesPersonsService;
        }

        /// <summary>
        /// Adds or updates single relation [Salesperson - District].
        /// </summary>
        /// <param name="salesPersonId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("{salesPersonId:guid}")]
        [HttpPut]
        [ResponseType(typeof(SalesPersonAddUpdateResponse))]
        public IHttpActionResult AddUpdate(Guid salesPersonId, [FromBody]SalesPersonAddUpdateRequest request)
        {
            if (request == null)
            {
                Logger.Write("Request was NOT binded!");

                return BadRequest();
            }

            SalesPersonAddUpdateResponse salesPersonAddUpdateResponse;
            try
            {
                salesPersonAddUpdateResponse = _salesPersonsService.AddUpdate(salesPersonId, request.DistrictId, request.IsPrimary, request.IsSecondary);
            }
            catch (NotFoundSalesPersonException)
            {
                return NotFound();
            }
            catch (NotFoundDistrictException)
            {
                return BadRequest("The district from the request was not found.");
            }
            catch (OnlyOnePrimarySalesPersonPerDistrictException)
            {
                return BadRequest("The district from the request is already having a primary sales person.");
            }

            return Ok(salesPersonAddUpdateResponse);           
        }

        /// <summary>
        /// Gets all salespersons.
        /// </summary>
        /// <returns></returns>
        [Route("")]
        [HttpGet]
        [ResponseType(typeof(SalesPersonListResponse))]
        public IHttpActionResult GetAll()
        {
            SalesPersonListResponse responseModel = _salesPersonsService.GetAll();

            return Ok(responseModel);
        }
    }
}
