﻿using System.Web.Http;
using Warehouse.WebService.Services;

namespace Warehouse.WebService.Controllers
{
    public abstract class ApiControllerBase : ApiController
    {
        protected readonly ILogger Logger;

        protected ApiControllerBase(ILogger logger)
        {
            Logger = logger;
        }
    }
}
