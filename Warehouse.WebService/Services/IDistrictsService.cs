﻿using System;
using Warehouse.WebService.Models;

namespace Warehouse.WebService.Services
{
    public interface IDistrictsService
    {
        DistrictResponse Get(Guid districtId);

        DistrictListResponse GetAll();
    }
}
