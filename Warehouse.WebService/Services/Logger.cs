﻿using System;
using System.Threading;

namespace Warehouse.WebService.Services
{
    public class Logger : ILogger
    {
        private readonly string _dateTime = $"{DateTime.UtcNow:yyyy-MM-dd HH:mm:ss ffff}";

        public void Write(string message, params object[] args)
        {
            var threadId = GetCurrentThreadId();

            if (args.Length == 0)
            {
                Console.WriteLine($"[{threadId}] [{_dateTime}] :" + message);
            }
            else
            {
                Console.WriteLine($"[{threadId}] [{_dateTime}] :" + message, args);
            }
        }

        public void Write(Exception ex)
        {
            var threadId = GetCurrentThreadId();

            Console.WriteLine($"[{threadId}] [{_dateTime}] :" + Environment.NewLine +
                              "Message: " + ex.Message + Environment.NewLine + 
                              "StackTrace: " + ex.StackTrace);
        }

        private int GetCurrentThreadId()
        {
            Thread thread = Thread.CurrentThread;

            return thread.ManagedThreadId;
        }
    }
}
