﻿using System;
using Warehouse.BusinessLogic.Models;
using Warehouse.WebService.Models;

namespace Warehouse.WebService.Services
{
    public interface ISalesPersonsService
    {
        SalesPersonAddUpdateResponse AddUpdate(Guid salesPersonId, Guid districtId, bool isPrimary, bool isSecondary);

        SalesPersonListResponse GetAll();
    }
}
