﻿using System;

namespace Warehouse.WebService.Services
{
    public interface ILogger
    {
        void Write(string message, params object[] args);
        void Write(Exception ex);
    }
}
