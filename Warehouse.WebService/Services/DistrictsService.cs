﻿using System;
using System.Collections.Generic;
using Warehouse.DataAccess;
using Warehouse.DataAccess.Entities;
using Warehouse.WebService.Exceptions;
using Warehouse.WebService.Models;

namespace Warehouse.WebService.Services
{
    public class DistrictsService : ServiceBase, IDistrictsService
    {
        private readonly IDistrictRepository _districtRepository;
        private readonly IStoreRepository _storeRepository;
        private readonly ISalesPersonRepository _salesPersonRepository;

        
        public DistrictsService(IDistrictRepository districtRepository, IStoreRepository storeRepository, 
                                ISalesPersonRepository salesPersonRepository, ILogger logger) : base(logger)
        {
            _districtRepository = districtRepository;
            _storeRepository = storeRepository;
            _salesPersonRepository = salesPersonRepository;   
        }
        
        public DistrictResponse Get(Guid districtId)
        {
            District dbDistrict = _districtRepository.GetDistrict(districtId);

            if(dbDistrict != null)
            {
                SalesPerson dbDPrimarySalesPerson = _salesPersonRepository.GetPrimarySalesPersonByDistrictId(dbDistrict.Id);
                List<SalesPerson> dbSecondarySalesPersons = _salesPersonRepository.GetSecondarySalesPersonsByDistrictId(dbDistrict.Id);
                List<Store> dbStores = _storeRepository.GetStoresByDistrictId(dbDistrict.Id);

                return new DistrictResponse(dbDistrict.Id, dbDistrict.Name, dbDPrimarySalesPerson, dbSecondarySalesPersons, dbStores);
            }

            throw new NotFoundDistrictException();
        }

        public DistrictListResponse GetAll()
        {
            List<District> dbDistricts = _districtRepository.GetAllDistricts();

            return new DistrictListResponse(dbDistricts);
        }
    }
}
