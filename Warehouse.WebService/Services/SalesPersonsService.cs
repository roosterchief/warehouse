﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Warehouse.BusinessLogic.Models;
using Warehouse.DataAccess;
using Warehouse.DataAccess.Entities;
using Warehouse.WebService.Exceptions;
using Warehouse.WebService.Models;

namespace Warehouse.WebService.Services
{
    public class SalesPersonsService : ServiceBase, ISalesPersonsService
    {
        private readonly ISalesPersonRepository _salesPersonRepository;
        private readonly IDistrictRepository _districtRepository;

        public SalesPersonsService(ISalesPersonRepository salesPersonRepository, IDistrictRepository districtRepository, ILogger logger) : base(logger)
        {
            _salesPersonRepository = salesPersonRepository;
            _districtRepository = districtRepository;
        }

        public SalesPersonAddUpdateResponse AddUpdate(Guid salesPersonId, Guid districtId, bool isPrimary, bool isSecondary)
        {
            if (_salesPersonRepository.GetSalesPerson(salesPersonId) == null) throw new NotFoundSalesPersonException();
            if (_districtRepository.GetDistrict(districtId) == null) throw new NotFoundDistrictException();

            if (!isPrimary && !isSecondary)
            {
                Logger.Write("This is actually a delete request.");

                _salesPersonRepository.RemoveSalesPersonDistrictRelation(salesPersonId, districtId);
            }
            else
            {
                Logger.Write("This is an update request.");

                try
                {
                    _salesPersonRepository.AddUpdateSalesPersonDistrictRelation(salesPersonId, districtId, isPrimary, isSecondary);
                }
                catch (SqlException ex)
                {
                    if(ex.Message.Contains("District has already primary salesperson. Only one primary salesperson per district is allowed."))
                    {
                        throw new OnlyOnePrimarySalesPersonPerDistrictException();
                    }

                    throw;
                }
            }

            return new SalesPersonAddUpdateResponse(salesPersonId, districtId, isPrimary, isSecondary);
        }

        public SalesPersonListResponse GetAll()
        {
            List<SalesPerson> dbSalesPersons = _salesPersonRepository.GetAll();

            return new SalesPersonListResponse(dbSalesPersons);
        }
    }
}
