﻿using System;
using System.Web.Http;
using Newtonsoft.Json.Serialization;
using Owin;
using Swashbuckle.Application;
using Warehouse.WebService.Configuration;

namespace Warehouse.WebService
{
    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            // Configure Web API for self-host
            HttpConfiguration config = new HttpConfiguration();
            
            // Attribute routing
            config.MapHttpAttributeRoutes();

            // Dependency Injection
            HandlAutofacSetup(appBuilder, config);          

            // Swagger
            config.EnableSwagger(c =>
            {
                c.SingleApiVersion("v1", "WarehouseApi");
                c.IncludeXmlComments($@"{AppDomain.CurrentDomain.BaseDirectory}\Warehouse.WebService.XML");
            }).EnableSwaggerUi();

            // Formatters
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            // Cors
            config.EnableCors();

            appBuilder.UseWebApi(config);
        }

        public virtual void HandlAutofacSetup(IAppBuilder appBuilder, HttpConfiguration config)
        {
            AutofacConfig.Setup(appBuilder,config); // Will be overwritten in the integration tests...
        }
    }
}
