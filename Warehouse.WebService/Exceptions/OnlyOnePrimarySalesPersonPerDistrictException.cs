﻿using System;

namespace Warehouse.WebService.Exceptions
{
    public class OnlyOnePrimarySalesPersonPerDistrictException : Exception
    {
    }
}
