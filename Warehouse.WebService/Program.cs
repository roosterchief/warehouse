﻿using System;
using System.Configuration;
using Microsoft.Owin.Hosting;

namespace Warehouse.WebService
{
    class Program
    {
        //Install-Package Microsoft.AspNet.WebApi.OwinSelfHost
        //Install-Package Swashbuckle.Core
        //Install-Package Autofac.WebApi2.Owin
        //Install-Package Microsoft.AspNet.WebApi.Cors
        static void Main()
        {
            //https://docs.microsoft.com/en-us/sql/reporting-services/install-windows/url-reservation-syntax-ssrs-configuration-manager

            string host = ConfigurationManager.AppSettings["host"];
            string port = ConfigurationManager.AppSettings["port"];

            string baseAddress = $"http://{host}:{port}";
            
            using (WebApp.Start<Startup>(url: baseAddress))
            {
                Console.WriteLine($"Server started @ {baseAddress} ...");
                Console.WriteLine("Press ENTER to stop.");
                Console.ReadLine();
            }
        }
    }
}
