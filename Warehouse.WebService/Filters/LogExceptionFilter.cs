﻿using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Filters;
using Autofac.Integration.WebApi;
using Warehouse.WebService.Services;

namespace Warehouse.WebService.Filters
{
    //https://www.exceptionnotfound.net/the-asp-net-web-api-exception-handling-pipeline-a-guided-tour/
    public class LogExceptionFilter : ExceptionFilterAttribute, IAutofacExceptionFilter
    {
        private readonly ILogger _logger;
        public LogExceptionFilter(ILogger logger)
        {
            _logger = logger;
        }

        public override void OnException(HttpActionExecutedContext context)
        {
            if (context.Exception is SqlException)
            {
                // We are trying to manipulate a record which is conflicting the database integrity.
                // We will consider this as a 'Conflict' and this status code is returned.
                if (context.Exception.Message.Contains("The INSERT statement conflicted with the FOREIGN KEY")) 
                {
                    var response = new HttpResponseMessage(HttpStatusCode.Conflict)
                    {
                        Content = new StringContent(context.Exception.Message),
                        ReasonPhrase = "Conflict"
                    };

                    LogAndThrowException(context, response);
                }

                // We are interested to see what kind of other SqlExceptions are. 
                // So far the situation can be considered as 'BadRequest' until further investigation 
                // and this status code is returned.
                else
                {
                    var response = new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        Content = new StringContent(context.Exception.Message),
                        ReasonPhrase = "Bad Request"
                    };

                    LogAndThrowException(context, response);
                }
            }

            // We really did not expect any exception beyound this point. 
            // Everything else can be considered as 'InternalServerError' and this status code is returned.
            else
            {
                var response = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(context.Exception.Message),
                    ReasonPhrase = "Internal Server Error"
                };

                LogAndThrowException(context, response);
            }
        }

        private void LogAndThrowException(HttpActionExecutedContext context, HttpResponseMessage response)
        {
            _logger.Write(context.Exception);
            
            throw new HttpResponseException(response);
        }
    }
}
