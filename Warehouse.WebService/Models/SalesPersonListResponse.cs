﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Warehouse.DataAccess.Entities;

namespace Warehouse.WebService.Models
{
    public class SalesPersonListResponse
    {
        public SalesPersonListResponse(List<SalesPerson> salesPersons)
        {
            SalesPersons = salesPersons;
        }

        [JsonProperty]
        public List<SalesPerson> SalesPersons { get; private set; }
    }
}
