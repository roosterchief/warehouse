﻿using System;
using Newtonsoft.Json;

namespace Warehouse.BusinessLogic.Models
{
    public class SalesPersonAddUpdateResponse
    {
        public SalesPersonAddUpdateResponse(Guid id, Guid districtId, bool isPrimary, bool isSecondary)
        {
            Id = id;
            DistrictId = districtId;
            IsPrimary = isPrimary;
            IsSecondary = isSecondary;
        }

        [JsonProperty]
        public Guid Id { get; private set; }

        [JsonProperty]
        public Guid DistrictId { get; private set; }

        [JsonProperty]
        public bool IsPrimary { get; private set; }

        [JsonProperty]
        public bool IsSecondary { get; private set; }
    }
}
