﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Warehouse.DataAccess.Entities;

namespace Warehouse.WebService.Models
{
    public class DistrictListResponse
    {
        public DistrictListResponse(List<District> districts)
        {
            Districts = districts;
        }

        [JsonProperty]
        public List<District> Districts { get; private set; }
    }
}
