﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Warehouse.DataAccess.Entities;

namespace Warehouse.WebService.Models
{
    public class DistrictResponse
    {
        public DistrictResponse(Guid districtId, string name, SalesPerson primarySalesPerson, 
                                List<SalesPerson> secondarySalesPersons, List<Store> stores)
        {
            DistrictId = districtId;
            Name = name;
            PrimarySalesPerson = primarySalesPerson;
            SecondarySalesPersons = secondarySalesPersons;
            Stores = stores;
        }

        [JsonProperty]
        public Guid DistrictId { get; private set; }

        [JsonProperty]
        public string Name { get; private set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public SalesPerson PrimarySalesPerson { get; private set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<SalesPerson> SecondarySalesPersons { get; private set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Store> Stores { get; private set; }
    }
}
