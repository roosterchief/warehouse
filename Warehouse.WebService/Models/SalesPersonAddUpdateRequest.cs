﻿using System;
using Newtonsoft.Json;

namespace Warehouse.WebService.Models
{
    public class SalesPersonAddUpdateRequest
    {
        [JsonProperty]
        public Guid DistrictId { get; set; }

        [JsonProperty]
        public bool IsPrimary { get; set; }

        [JsonProperty]
        public bool IsSecondary { get; set; }
    }
}
