﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.Owin;
using Warehouse.WebService.Services;

namespace Warehouse.WebService.Middleware
{
    //http://www.lightinject.net/webapirequestlogging/
    public class RequestLoggingMiddleware : OwinMiddleware
    {
        private readonly ILogger _logger;

        public RequestLoggingMiddleware(OwinMiddleware next, ILogger logger) : base(next)
        {
            _logger = logger;
        }

        public override async Task Invoke(IOwinContext context)
        {
            await Measure(context).ConfigureAwait(false);
        }

        private async Task Measure(IOwinContext context)
        {
            var stopWath = Stopwatch.StartNew();

            await Next.Invoke(context).ConfigureAwait(false);

            stopWath.Stop();

            _logger.Write($"Request {context.Request.Method} {context.Request.Uri.PathAndQuery} took {stopWath.ElapsedMilliseconds} ms");
        }
    }
}
