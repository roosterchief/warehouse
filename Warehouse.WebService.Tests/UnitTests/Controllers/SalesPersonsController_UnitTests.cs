﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Warehouse.BusinessLogic.Models;
using Warehouse.DataAccess.Entities;
using Warehouse.WebService.Controllers;
using Warehouse.WebService.Exceptions;
using Warehouse.WebService.Models;
using Warehouse.WebService.Services;

namespace Warehouse.WebService.Tests.UnitTests.Controllers
{
    [TestClass]
    public class SalesPersonsController_UnitTests : UnitTestsBase
    {
        private SalesPersonsController _target;

        [TestInitialize]
        public override void Init()
        {
            // arrange
            base.Init();

            MockSalesPersonsSercice = new Mock<ISalesPersonsService>();
            MockSalesPersonsSercice.Setup(m => m.GetAll()).Returns(new SalesPersonListResponse(new List<SalesPerson> { SalesPerson1, SalesPerson2 }));
            MockSalesPersonsSercice.Setup(m => m.AddUpdate(SALES_PERSON_ID_1, DISTRICT_ID, true, true)).Returns(new SalesPersonAddUpdateResponse(SALES_PERSON_ID_1, DISTRICT_ID, true, true));
            MockSalesPersonsSercice.Setup(m => m.AddUpdate(SALES_PERSON_ID_1, DISTRICT_ID, false, true)).Returns(new SalesPersonAddUpdateResponse(SALES_PERSON_ID_1, DISTRICT_ID, false, true));
            MockSalesPersonsSercice.Setup(m => m.AddUpdate(SALES_PERSON_ID_1, DISTRICT_ID, true, false)).Returns(new SalesPersonAddUpdateResponse(SALES_PERSON_ID_1, DISTRICT_ID, true, false));
            MockSalesPersonsSercice.Setup(m => m.AddUpdate(SALES_PERSON_ID_1, DISTRICT_ID, false, false)).Returns(new SalesPersonAddUpdateResponse(SALES_PERSON_ID_1, DISTRICT_ID, false, false));
            MockSalesPersonsSercice.Setup(m => m.AddUpdate(UNKNOWN_SALES_PERSON_ID, DISTRICT_ID, false, false)).Throws(new NotFoundSalesPersonException());
            MockSalesPersonsSercice.Setup(m => m.AddUpdate(SALES_PERSON_ID_1, UNKNOWN_DISTRICT_ID, false, false)).Throws(new NotFoundDistrictException());
            MockSalesPersonsSercice.Setup(m => m.AddUpdate(SALES_PERSON_ID_2, DISTRICT_ID, true, false)).Throws(new OnlyOnePrimarySalesPersonPerDistrictException());

            MockLogger = new Mock<ILogger>();
            MockLogger.Setup(x => x.Write(It.IsAny<string>()));

            _target = new SalesPersonsController(MockSalesPersonsSercice.Object, MockLogger.Object);
        }

        [TestMethod]
        public void GetAll_ReturnsExpectedResult()
        {
            // act
            IHttpActionResult result = _target.GetAll();

            // assert
            SalesPersonListResponse expected = new SalesPersonListResponse(new List<SalesPerson> { SalesPerson1, SalesPerson2 });
            Assert.IsNotNull(result as OkNegotiatedContentResult<SalesPersonListResponse>);
            AssertExpectedAndResult(expected, (result as OkNegotiatedContentResult<SalesPersonListResponse>).Content);
        }

        [TestMethod]
        public void AddUpdate_WhenKnownSalesPersonAndDistrict_ReturnsExpectedResult()
        {
            // act
            IHttpActionResult result = _target.AddUpdate(SALES_PERSON_ID_1, new SalesPersonAddUpdateRequest { DistrictId = DISTRICT_ID, IsPrimary = true, IsSecondary = true });

            // assert
            SalesPersonAddUpdateResponse expected = new SalesPersonAddUpdateResponse(SALES_PERSON_ID_1, DISTRICT_ID, true, true);
            Assert.IsNotNull(result as OkNegotiatedContentResult<SalesPersonAddUpdateResponse>);
            AssertExpectedAndResult(expected, (result as OkNegotiatedContentResult<SalesPersonAddUpdateResponse>).Content);
        }

        [TestMethod]
        public void AddUpdate_WhenKnownSalesPersonAndDistrict2_ReturnsExpectedResult()
        {
            // act
            IHttpActionResult result = _target.AddUpdate(SALES_PERSON_ID_1, new SalesPersonAddUpdateRequest { DistrictId = DISTRICT_ID, IsPrimary = true, IsSecondary = false });

            // assert
            SalesPersonAddUpdateResponse expected = new SalesPersonAddUpdateResponse(SALES_PERSON_ID_1, DISTRICT_ID, true, false);
            Assert.IsNotNull(result as OkNegotiatedContentResult<SalesPersonAddUpdateResponse>);
            AssertExpectedAndResult(expected, (result as OkNegotiatedContentResult<SalesPersonAddUpdateResponse>).Content);
        }

        [TestMethod]
        public void AddUpdate_WhenKnownSalesPersonAndDistrict3_ReturnsExpectedResult()
        {
            // act
            IHttpActionResult result = _target.AddUpdate(SALES_PERSON_ID_1, new SalesPersonAddUpdateRequest { DistrictId = DISTRICT_ID, IsPrimary = false, IsSecondary = true });

            // assert
            SalesPersonAddUpdateResponse expected = new SalesPersonAddUpdateResponse(SALES_PERSON_ID_1, DISTRICT_ID, false, true);
            Assert.IsNotNull(result as OkNegotiatedContentResult<SalesPersonAddUpdateResponse>);
            AssertExpectedAndResult(expected, (result as OkNegotiatedContentResult<SalesPersonAddUpdateResponse>).Content);
        }

        [TestMethod]
        public void AddUpdate_WhenKnownSalesPersonAndDistrict4_ReturnsExpectedResult()
        {
            // act
            IHttpActionResult result = _target.AddUpdate(SALES_PERSON_ID_1, new SalesPersonAddUpdateRequest { DistrictId = DISTRICT_ID, IsPrimary = false, IsSecondary = false });

            // assert
            SalesPersonAddUpdateResponse expected = new SalesPersonAddUpdateResponse(SALES_PERSON_ID_1, DISTRICT_ID, false, false);
            Assert.IsNotNull(result as OkNegotiatedContentResult<SalesPersonAddUpdateResponse>);
            AssertExpectedAndResult(expected, (result as OkNegotiatedContentResult<SalesPersonAddUpdateResponse>).Content);
        }

        [TestMethod]
        public void AddUpdate_WhenUnknownSalesPersonId_ReturnsNotFoundResult()
        {
            // act
            IHttpActionResult result = _target.AddUpdate(UNKNOWN_SALES_PERSON_ID, new SalesPersonAddUpdateRequest { DistrictId = DISTRICT_ID, IsPrimary = false, IsSecondary = false });

            // assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        [TestMethod]
        public void AddUpdate_WhenUnknownDistrictId_ReturnsBadRequestResult()
        {
            // act
            IHttpActionResult result = _target.AddUpdate(SALES_PERSON_ID_1, new SalesPersonAddUpdateRequest { DistrictId = UNKNOWN_DISTRICT_ID, IsPrimary = false, IsSecondary = false });

            // assert
            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void AddUpdate_WhenDistrictHastAlreadyPrimarySalesPerson_ReturnsBadRequestResult()
        {
            // act 
            IHttpActionResult result = _target.AddUpdate(SALES_PERSON_ID_2, new SalesPersonAddUpdateRequest { DistrictId = DISTRICT_ID, IsPrimary = true, IsSecondary = false });

            // assert
            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));
        }
    }
}
