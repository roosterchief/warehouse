﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Warehouse.DataAccess.Entities;
using Warehouse.WebService.Controllers;
using Warehouse.WebService.Exceptions;
using Warehouse.WebService.Models;
using Warehouse.WebService.Services;

namespace Warehouse.WebService.Tests.UnitTests.Controllers
{
    [TestClass]
    public class DistrictsController_UnitTests : UnitTestsBase
    {
        private DistrictsController _target;

        [TestInitialize]
        public override void Init()
        {
            // arrange
            base.Init();

            MockDistrictsService = new Mock<IDistrictsService>(MockBehavior.Strict);
            MockDistrictsService.Setup(m => m.GetAll()).Returns(new DistrictListResponse(new List<District>{District, District2}));
            MockDistrictsService.Setup(m => m.Get(DISTRICT_ID)).Returns(new DistrictResponse(DISTRICT_ID, "District 9", SalesPerson1, new List<SalesPerson> { SalesPerson1, SalesPerson2 }, new List<Store> {Store1, Store2 }));
            MockDistrictsService.Setup(m => m.Get(UNKNOWN_DISTRICT_ID)).Throws(new NotFoundDistrictException());

            MockLogger = new Mock<ILogger>();
            MockLogger.Setup(x => x.Write(It.IsAny<string>()));

            _target = new DistrictsController(MockDistrictsService.Object, MockLogger.Object);
        }

        [TestMethod]
        public void GetAll_ReturnsExpectedResult()
        {
            // act
            IHttpActionResult result = _target.GetAll();

            // assert
            DistrictListResponse expected = new DistrictListResponse(new List<District> { District, District2 });
            Assert.IsNotNull(result as OkNegotiatedContentResult<DistrictListResponse>);
            AssertExpectedAndResult(expected, (result as OkNegotiatedContentResult<DistrictListResponse>).Content);
        }

        [TestMethod]
        public void Get_WhentKnownDistrictId_ReturnsExpectedResult()
        {
            // act
            IHttpActionResult result = _target.Get(DISTRICT_ID);

            // assert
            DistrictResponse expected = new DistrictResponse(DISTRICT_ID, "District 9", SalesPerson1, new List<SalesPerson> { SalesPerson1, SalesPerson2 }, new List<Store> { Store1, Store2 });
            Assert.IsNotNull(result as OkNegotiatedContentResult<DistrictResponse>);
            AssertExpectedAndResult(expected, (result as OkNegotiatedContentResult<DistrictResponse>).Content);
        }

        [TestMethod]
        public void Get_WhenUnknownDistrictId_ReturnsNotFoundResult()
        {
            // act + assert
            IHttpActionResult result = _target.Get(UNKNOWN_DISTRICT_ID);

            // assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }
    }
}
