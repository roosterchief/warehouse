﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Warehouse.BusinessLogic.Models;
using Warehouse.DataAccess;
using Warehouse.DataAccess.Entities;
using Warehouse.WebService.Exceptions;
using Warehouse.WebService.Models;
using Warehouse.WebService.Services;

namespace Warehouse.WebService.Tests.UnitTests.Services
{
    [TestClass]
    public class SalesPersonsService_UnitTests : UnitTestsBase
    {
        private ISalesPersonsService _target;

        [TestInitialize]
        public override void Init()
        {
            // arrange
            base.Init();

            MockSalesPersonRepository = new Mock<ISalesPersonRepository>();
            MockSalesPersonRepository.Setup(m => m.GetPrimarySalesPersonByDistrictId(It.IsIn(new List<Guid> { DISTRICT_ID }.AsEnumerable()))).Returns(SalesPerson1);
            MockSalesPersonRepository.Setup(m => m.GetSecondarySalesPersonsByDistrictId(It.IsIn(new List<Guid> { DISTRICT_ID, DISTRICT_ID_2 }.AsEnumerable()))).Returns(new List<SalesPerson> { SalesPerson1, SalesPerson2 });
            MockSalesPersonRepository.Setup(m => m.GetAll()).Returns(new List<SalesPerson> { SalesPerson1, SalesPerson2 });
            MockSalesPersonRepository.Setup(m => m.GetSalesPerson(SALES_PERSON_ID_1)).Returns(SalesPerson1);
            MockSalesPersonRepository.Setup(m => m.GetSalesPerson(UNKNOWN_SALES_PERSON_ID)).Returns((SalesPerson)null);

            MockDistrictRepository = new Mock<IDistrictRepository>();
            MockDistrictRepository.Setup(m => m.GetDistrict(DISTRICT_ID)).Returns(District);
            MockDistrictRepository.Setup(m => m.GetDistrict(UNKNOWN_DISTRICT_ID)).Returns((District) null);

            MockLogger = new Mock<ILogger>();
            MockLogger.Setup(x => x.Write(It.IsAny<string>()));

            _target = new SalesPersonsService(MockSalesPersonRepository.Object, MockDistrictRepository.Object, MockLogger.Object);
        }

        #region GetAll

        [TestMethod]
        public void GetAll_ReturnsExpectedResults()
        {
            // act
            SalesPersonListResponse result = _target.GetAll();

            // assert
            SalesPersonListResponse expected = new SalesPersonListResponse(new List<SalesPerson> {SalesPerson1, SalesPerson2});
            AssertExpectedAndResult(expected, result);
        }

        #endregion

        #region AddUpdate

        [TestMethod]
        public void AddUpdate_WhenIsPrimaryAndIsSecondaryTrue_ReturnsExpectedResults()
        {
            // act
            SalesPersonAddUpdateResponse result = _target.AddUpdate(SALES_PERSON_ID_1, DISTRICT_ID, true, true);

            // assert
            MockSalesPersonRepository.Verify(x => x.AddUpdateSalesPersonDistrictRelation(It.IsAny<Guid>(), It.IsAny<Guid>(), true, true), Times.Once);
            MockSalesPersonRepository.Verify(x => x.RemoveSalesPersonDistrictRelation(It.IsAny<Guid>(), It.IsAny<Guid>()), Times.Never);
            SalesPersonAddUpdateResponse expected = new SalesPersonAddUpdateResponse(SALES_PERSON_ID_1, DISTRICT_ID, true, true );
            AssertExpectedAndResult(result, expected);
        }

        [TestMethod]
        public void AddUpdate_WhenIsPrimaryTrueAndIsSecondaryFalse_ReturnsExpectedResults()
        {
            // act
            SalesPersonAddUpdateResponse result = _target.AddUpdate(SALES_PERSON_ID_1, DISTRICT_ID,  true, false);

            // assert
            MockSalesPersonRepository.Verify(x => x.AddUpdateSalesPersonDistrictRelation(It.IsAny<Guid>(), It.IsAny<Guid>(), true, false), Times.Once);
            MockSalesPersonRepository.Verify(x => x.RemoveSalesPersonDistrictRelation(It.IsAny<Guid>(), It.IsAny<Guid>()), Times.Never);
            SalesPersonAddUpdateResponse expected = new SalesPersonAddUpdateResponse(SALES_PERSON_ID_1, DISTRICT_ID, true, false);
            AssertExpectedAndResult(result, expected);
        }

        [TestMethod]
        public void AddUpdate_WhenIsPrimaryFalseAndIsSecondaryTrue_ReturnsExpectedResults()
        {
            // act
            SalesPersonAddUpdateResponse result = _target.AddUpdate(SALES_PERSON_ID_1, DISTRICT_ID, false, true);

            // assert
            MockSalesPersonRepository.Verify(x => x.AddUpdateSalesPersonDistrictRelation(It.IsAny<Guid>(), It.IsAny<Guid>(), false, true), Times.Once);
            MockSalesPersonRepository.Verify(x => x.RemoveSalesPersonDistrictRelation(It.IsAny<Guid>(), It.IsAny<Guid>()), Times.Never);
            SalesPersonAddUpdateResponse expected = new SalesPersonAddUpdateResponse(SALES_PERSON_ID_1, DISTRICT_ID, false, true);
            AssertExpectedAndResult(result, expected);
        }

        [TestMethod]
        public void AddUpdate_WhenIsPrimaryFalseAndIsSecondaryFalse_ReturnsExpectedResults()
        {
            // act
            SalesPersonAddUpdateResponse result = _target.AddUpdate(SALES_PERSON_ID_1, DISTRICT_ID, false, false);

            // assert
            MockSalesPersonRepository.Verify(x => x.AddUpdateSalesPersonDistrictRelation(It.IsAny<Guid>(), It.IsAny<Guid>(), false, false), Times.Never);
            MockSalesPersonRepository.Verify(x => x.RemoveSalesPersonDistrictRelation(It.IsAny<Guid>(), It.IsAny<Guid>()), Times.Once);
            SalesPersonAddUpdateResponse expected = new SalesPersonAddUpdateResponse(SALES_PERSON_ID_1, DISTRICT_ID, false, false);
            AssertExpectedAndResult(result, expected);
        }

        [TestMethod]
        [ExpectedException(typeof(NotFoundSalesPersonException))]
        public void AddUpdate_WhenUnknownSalesPersonId_ThrowsNotFoundSalesPersonException()
        {
            // act + asserrt
            _target.AddUpdate(UNKNOWN_SALES_PERSON_ID, DISTRICT_ID, true, false);
        }

        [TestMethod]
        [ExpectedException(typeof(NotFoundDistrictException))]
        public void AddUpdate_WhenUnknownDistrictId_ThrowsNotFoundDistrictException()
        {
            // act + asserrt
            _target.AddUpdate(SALES_PERSON_ID_1, UNKNOWN_DISTRICT_ID, true, false);
        }
        #endregion
    }
}
