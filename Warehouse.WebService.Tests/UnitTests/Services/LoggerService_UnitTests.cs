﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Warehouse.WebService.Services;

namespace Warehouse.WebService.Tests.UnitTests.Services
{
    [TestClass]
    public class LoggerService_UnitTests
    {
        [TestMethod]
        public void ILogger_WriteString()
        {
            // arrange
            var mockLogger = new Mock<ILogger>();

            // act
            mockLogger.Object.Write("Test message");

            // assert
            mockLogger.Verify(x => x.Write(It.IsAny<string>()), Times.Once);
            mockLogger.Verify(x => x.Write(It.IsAny<Exception>()), Times.Never);
        }

        [TestMethod]
        public void ILogger_WriteException()
        {
            // arrange
            var mockLogger = new Mock<ILogger>();

            // act
            mockLogger.Object.Write(new Exception());

            // assert
            mockLogger.Verify(x => x.Write(It.IsAny<Exception>()), Times.Once);
            mockLogger.Verify(x => x.Write(It.IsAny<string>()), Times.Never);
        }
    }
}
