﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Warehouse.DataAccess;
using Warehouse.DataAccess.Entities;
using Warehouse.WebService.Exceptions;
using Warehouse.WebService.Models;
using Warehouse.WebService.Services;

namespace Warehouse.WebService.Tests.UnitTests.Services
{
    [TestClass]
    public class DistrictsService_UnitTests : UnitTestsBase
    {
        private IDistrictsService _target;

        [TestInitialize]
        public override void Init()
        {
            // arrange
            base.Init();

            MockDistrictRepository = new Mock<IDistrictRepository>();
            MockDistrictRepository.Setup(m => m.GetDistrict(DISTRICT_ID)).Returns(District);
            MockDistrictRepository.Setup(m => m.GetDistrict(UNKNOWN_DISTRICT_ID)).Returns((District)null);
            MockDistrictRepository.Setup(m => m.GetAllDistricts()).Returns(new List<District> { District, District2 });

            MockStoreRepository = new Mock<IStoreRepository>();
            MockStoreRepository.Setup(m => m.GetStoresByDistrictId(It.IsIn(new List<Guid> { DISTRICT_ID, DISTRICT_ID_2 }.AsEnumerable()))).Returns(new List<Store> { Store1, Store2 });

            MockSalesPersonRepository = new Mock<ISalesPersonRepository>();
            MockSalesPersonRepository.Setup(m => m.GetPrimarySalesPersonByDistrictId(It.IsIn(new List<Guid> { DISTRICT_ID, DISTRICT_ID_2 }.AsEnumerable()))).Returns(SalesPerson1);
            MockSalesPersonRepository.Setup(m => m.GetSecondarySalesPersonsByDistrictId(It.IsIn(new List<Guid> { DISTRICT_ID, DISTRICT_ID_2 }.AsEnumerable()))).Returns(new List<SalesPerson> { SalesPerson1, SalesPerson2 });

            MockLogger = new Mock<ILogger>();
            MockLogger.Setup(x => x.Write(It.IsAny<string>()));

            _target = new DistrictsService(MockDistrictRepository.Object, MockStoreRepository.Object, MockSalesPersonRepository.Object, MockLogger.Object);
        }

        #region Get

        [TestMethod]
        public void Get_WhenKnownDistictId_ReturnsExpectedResult()
        {
            // act
            DistrictResponse result = _target.Get(DISTRICT_ID);

            // assert
            DistrictResponse expected = new DistrictResponse(DISTRICT_ID, "District 9", SalesPerson1, new List<SalesPerson> { SalesPerson1, SalesPerson2 }, new List<Store> { Store1, Store2 });
            AssertExpectedAndResult(expected, result);
        }

        [TestMethod]
        [ExpectedException(typeof(NotFoundDistrictException))]
        public void Get_WhenUnknownDistrictId_ThrowsNotFoundDistrictException()
        {
            // act + assert
            _target.Get(UNKNOWN_DISTRICT_ID);
        }

        #endregion

        #region GetAll

        [TestMethod]
        public void GetAll_ReturnsExpectedResult()
        {
            // act
            DistrictListResponse result = _target.GetAll();

            // assert
            DistrictListResponse expected = new DistrictListResponse(new List<District> { District, District2 });
            AssertExpectedAndResult(expected, result);
        }

        #endregion
    }
}
