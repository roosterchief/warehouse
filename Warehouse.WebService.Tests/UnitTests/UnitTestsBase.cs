﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Warehouse.BusinessLogic.Models;
using Warehouse.DataAccess;
using Warehouse.DataAccess.Entities;
using Warehouse.WebService.Models;
using Warehouse.WebService.Services;

namespace Warehouse.WebService.Tests.UnitTests
{

    public abstract class UnitTestsBase
    {
        protected readonly Guid UNKNOWN_DISTRICT_ID = Guid.NewGuid();
        protected readonly Guid DISTRICT_ID = Guid.NewGuid();
        protected readonly Guid DISTRICT_ID_2 = Guid.NewGuid();
        protected readonly Guid STORE_ID_1 = Guid.NewGuid();
        protected readonly Guid STORE_ID_2 = Guid.NewGuid();
        protected readonly Guid UNKNOWN_SALES_PERSON_ID = Guid.NewGuid();
        protected readonly Guid SALES_PERSON_ID_1 = Guid.NewGuid();
        protected readonly Guid SALES_PERSON_ID_2 = Guid.NewGuid();

        protected District District;
        protected District District2;
        protected Store Store1;
        protected Store Store2;
        protected SalesPerson SalesPerson1;
        protected SalesPerson SalesPerson2;

        protected Mock<IDistrictRepository> MockDistrictRepository;
        protected Mock<IStoreRepository> MockStoreRepository;
        protected Mock<ISalesPersonRepository> MockSalesPersonRepository;

        protected Mock<ISalesPersonsService> MockSalesPersonsSercice;
        protected Mock<IDistrictsService> MockDistrictsService;

        protected Mock<ILogger> MockLogger;

        [TestInitialize]
        public virtual void Init()
        {
            // arrange
            District = new District { Id = DISTRICT_ID, Name = "District 9" };
            District2 = new District { Id = DISTRICT_ID_2, Name = "Red District" };

            Store1 = new Store { Id = STORE_ID_1, Address = "Hell Holes Address", Name = "Mc Donalds" };
            Store2 = new Store { Id = STORE_ID_2, Address = "Hell Holes Address", Name = "Burger King" };

            SalesPerson1 = new SalesPerson { Id = SALES_PERSON_ID_1, FirstName = "John", LastName = "Doe", Initials = "JDO" };
            SalesPerson2 = new SalesPerson { Id = SALES_PERSON_ID_2, FirstName = "Captain", LastName = "America", Initials = "CAM" };
        }

        protected void AssertExpectedAndResult(DistrictResponse expected, DistrictResponse result)
        {
            if (expected == null) throw new ArgumentNullException(nameof(expected));
            if (result == null) throw new ArgumentNullException(nameof(result));

            Assert.IsTrue(expected.DistrictId == result.DistrictId);
            Assert.IsTrue(expected.Name == result.Name);

            AssertExpectedAndResult(expected.PrimarySalesPerson, result.PrimarySalesPerson);

            Assert.IsTrue(expected.SecondarySalesPersons.Count == result.SecondarySalesPersons.Count);
            for (int i = 0; i < expected.SecondarySalesPersons.Count; i++)
            {
                AssertExpectedAndResult(expected.SecondarySalesPersons[i], result.SecondarySalesPersons[i]);
            }

            Assert.IsTrue(expected.Stores.Count == result.Stores.Count);
            for (int i = 0; i < expected.Stores.Count; i++)
            {
                AssertExpectedAndResult(expected.Stores[i], result.Stores[i]);
            }
        }

        protected void AssertExpectedAndResult(District expected, District result)
        {
            if (expected == null) throw new ArgumentNullException(nameof(expected));
            if (result == null) throw new ArgumentNullException(nameof(result));

            Assert.IsTrue(expected.Id == result.Id);
            Assert.IsTrue(expected.Name == result.Name);
        }

        protected void AssertExpectedAndResult(SalesPersonListResponse expected, SalesPersonListResponse result)
        {
            if (expected == null) throw new ArgumentNullException(nameof(expected));
            if (result == null) throw new ArgumentNullException(nameof(result));

            if (expected.SalesPersons == null) throw new ArgumentNullException(nameof(expected.SalesPersons));
            if (result.SalesPersons == null) throw new ArgumentNullException(nameof(result.SalesPersons));

            Assert.IsTrue(expected.SalesPersons.Count == result.SalesPersons.Count);

            for (int i = 0; i < expected.SalesPersons.Count; i++)
            {
                AssertExpectedAndResult(expected.SalesPersons[i], result.SalesPersons[i]);
            }
        }

        protected void AssertExpectedAndResult(DistrictListResponse expected, DistrictListResponse result)
        {
            if (expected == null) throw new ArgumentNullException(nameof(expected));
            if (result == null) throw new ArgumentNullException(nameof(result));

            if (expected.Districts == null) throw new ArgumentNullException(nameof(expected.Districts));
            if (result.Districts == null) throw new ArgumentNullException(nameof(result.Districts));

            Assert.IsTrue(expected.Districts.Count == result.Districts.Count);

            for (int i = 0; i < expected.Districts.Count; i++)
            {
                AssertExpectedAndResult(expected.Districts[i], result.Districts[i]);
            }
        }

        protected void AssertExpectedAndResult(Store expected, Store result)
        {
            if (expected == null) throw new ArgumentNullException(nameof(expected));
            if (result == null) throw new ArgumentNullException(nameof(result));

            Assert.IsTrue(expected.Id == result.Id);
            Assert.IsTrue(expected.Name == result.Name);
        }

        protected void AssertExpectedAndResult(SalesPerson expected, SalesPerson result)
        {
            if (expected == null) throw new ArgumentNullException(nameof(expected));
            if (result == null) throw new ArgumentNullException(nameof(result));

            Assert.IsTrue(expected.Id == result.Id);
            Assert.IsTrue(expected.FirstName == result.FirstName);
            Assert.IsTrue(expected.LastName == result.LastName);
            Assert.IsTrue(expected.Initials == result.Initials);
        }

        protected void AssertExpectedAndResult(SalesPersonAddUpdateResponse expected, SalesPersonAddUpdateResponse result)
        {
            if (expected == null) throw new ArgumentNullException(nameof(expected));
            if (result == null) throw new ArgumentNullException(nameof(result));

            Assert.IsTrue(expected.Id == result.Id);
            Assert.IsTrue(expected.DistrictId == result.DistrictId);
            Assert.IsTrue(expected.IsPrimary == result.IsPrimary);
            Assert.IsTrue(expected.IsSecondary == result.IsSecondary);
        }
    }
}
