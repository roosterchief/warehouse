﻿using Microsoft.Owin.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Warehouse.WebService.Tests.IntegrationTests
{
    public class IntegrationTestsBase
    {
        protected TestServer Server;

        [TestInitialize]
        public void FixtureInit()
        {
            // arragnge
            Server = TestServer.Create<TestStartup>();
        }

        [TestCleanup]
        public void FixtureDispose()
        {
            Server.Dispose();
        }
    }
}
