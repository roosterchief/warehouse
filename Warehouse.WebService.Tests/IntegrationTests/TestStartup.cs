﻿using System.Web.Http;
using Owin;

namespace Warehouse.WebService.Tests.IntegrationTests
{
    public class TestStartup : Startup
    {
        public override void HandlAutofacSetup(IAppBuilder appBuilder, HttpConfiguration config)
        {
            TestAutofacConfig.Setup(appBuilder, config);
        }
    }
}
