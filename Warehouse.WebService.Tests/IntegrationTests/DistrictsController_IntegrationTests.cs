﻿using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Warehouse.WebService.Tests.IntegrationTests
{
    [TestClass]
    public class DistrictsController_IntegrationTests : IntegrationTestsBase
    {
        [TestMethod]
        public void GET_districts_WhenEverythingIsOk_ReturnsExpectedResult()
        {
            // act
            var response = Server.HttpClient.GetAsync("api/districts").Result;
            var result = response.Content.ReadAsStringAsync().Result;

            // assert
            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK);
            Assert.IsTrue(result == "{\"districts\":[{\"id\":\"00000000-0000-0000-0000-000000000000\",\"name\":\"Test District\"}]}");
        }

        [TestMethod]
        public void GET_districts_WhenWrongUrl_ReturnsNotFound()
        {
            // act
            var response = Server.HttpClient.GetAsync("api/districts2").Result;
            var result = response.Content.ReadAsStringAsync().Result;

            // assert
            Assert.IsTrue(response.StatusCode == HttpStatusCode.NotFound);
            Assert.IsTrue(result == string.Empty);
        }

        [TestMethod]
        public void GET_district_WhenEverythingIsOk_ReturnsExpectedResult()
        {
            // act
            var response = Server.HttpClient.GetAsync("api/districts/00000000-0000-0000-0000-000000000000").Result;
            var result = response.Content.ReadAsStringAsync().Result;

            // assert
            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK);
            Assert.IsTrue(result == "{\"districtId\":\"00000000-0000-0000-0000-000000000000\",\"name\":\"Test District\",\"primarySalesPerson\":{\"id\":\"00000000-0000-0000-0000-000000000000\",\"firstName\":\"Test First Name\",\"lastName\":\"Test Last Name\",\"initials\":\"TT\"},\"secondarySalesPersons\":[],\"stores\":[]}");
        }

        [TestMethod]
        public void GET_district_WhenNotGuidValue1_ReturnsExpectedResult()
        {
            // act
            var response = Server.HttpClient.GetAsync("api/districts/00000000").Result;
            var result = response.Content.ReadAsStringAsync().Result;

            // assert
            Assert.IsTrue(response.StatusCode == HttpStatusCode.NotFound);
            Assert.IsTrue(result == string.Empty);
        }


        [TestMethod]
        public void GET_district_WhenNotGuidValue2_ReturnsExpectedResult()
        {
            // act
            var response = Server.HttpClient.GetAsync("api/districts/abc").Result;
            var result = response.Content.ReadAsStringAsync().Result;

            // assert
            Assert.IsTrue(response.StatusCode == HttpStatusCode.NotFound);
            Assert.IsTrue(result == string.Empty);
        }

        [TestMethod]
        public void GET_district_WhenNotGuidValue3_ReturnsExpectedResult()
        {
            // act
            var response = Server.HttpClient.GetAsync("api/districts/true").Result;
            var result = response.Content.ReadAsStringAsync().Result;

            // assert
            Assert.IsTrue(response.StatusCode == HttpStatusCode.NotFound);
            Assert.IsTrue(result == string.Empty);
        }

        [TestMethod]
        public void GET_district_WhenWrongUrl1_ReturnsNotFound()
        {
            // act
            var response = Server.HttpClient.GetAsync("api/districts2/00000000-0000-0000-0000-000000000000").Result;
            var result = response.Content.ReadAsStringAsync().Result;

            // assert
            Assert.IsTrue(response.StatusCode == HttpStatusCode.NotFound);
            Assert.IsTrue(result == string.Empty);
        }

        [TestMethod]
        public void GET_district_WhenWrongUrl2_ReturnsNotFound()
        {
            // act
            var response = Server.HttpClient.GetAsync("api/1/00000000-0000-0000-0000-000000000000").Result;
            var result = response.Content.ReadAsStringAsync().Result;

            // assert
            Assert.IsTrue(response.StatusCode == HttpStatusCode.NotFound);
            Assert.IsTrue(result == string.Empty);
        }
    }
}
