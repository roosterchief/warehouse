﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Owin;
using Warehouse.BusinessLogic.Models;
using Warehouse.DataAccess;
using Warehouse.DataAccess.Entities;
using Warehouse.WebService.Controllers;
using Warehouse.WebService.Filters;
using Warehouse.WebService.Middleware;
using Warehouse.WebService.Models;
using Warehouse.WebService.Services;

namespace Warehouse.WebService.Tests.IntegrationTests
{
    public class TestAutofacConfig
    {
        public static void Setup(IAppBuilder appBuilder, HttpConfiguration config)
        {
            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(Assembly.GetAssembly(typeof(ApiControllerBase)));

            builder.RegisterWebApiFilterProvider(config);

            // Filters
            builder.RegisterType<LogExceptionFilter>()
                .AsWebApiExceptionFilterFor<ApiControllerBase>()
                .InstancePerRequest();

            // Services
            builder.RegisterType<Logger>().As<ILogger>().InstancePerRequest();
            builder.RegisterType<TestDistrictsService>().As<IDistrictsService>().InstancePerRequest();
            builder.RegisterType<TestSalesPersonsService>().As<ISalesPersonsService>().InstancePerRequest();

            // Repositories
            builder.RegisterType<TestDistrictRepository>().As<IDistrictRepository>();
            builder.RegisterType<TestSalesPersonRepository>().As<ISalesPersonRepository>();
            builder.RegisterType<TestStoreRepository>().As<IStoreRepository>();

            // Middleware
            builder.RegisterType<RequestLoggingMiddleware>().InstancePerRequest();

            IContainer container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            appBuilder.UseAutofacMiddleware(container);
            appBuilder.UseAutofacWebApi(config);
        }
    }
    
    #region Repository

    public class TestDistrictRepository : IDistrictRepository
    {
        public List<District> GetAllDistricts()
        {
            return new List<District> { new District {Id = Guid.Empty, Name = "Test"} }; 
        }

        public District GetDistrict(Guid id)
        {
            return new District {Id = id, Name = "Test"};
        }
    }

    public class TestSalesPersonRepository : ISalesPersonRepository
    {
        public SalesPerson GetPrimarySalesPersonByDistrictId(Guid id)
        {
            return new SalesPerson {Id = Guid.Empty, FirstName = "Test First Name", LastName = "Test Last Name", Initials = "TT"};
        }

        public List<SalesPerson> GetSecondarySalesPersonsByDistrictId(Guid id)
        {
            return new List<SalesPerson> { new SalesPerson {Id = Guid.Empty, FirstName = "Test First Name", LastName = "Test Last Name", Initials = "TT"} };
        }

        public List<SalesPerson> GetAll()
        {
            return new List<SalesPerson> { new SalesPerson { Id = Guid.Empty, FirstName = "Test First Name", LastName = "Test Last Name", Initials = "TT" } };
        }

        public void AddUpdateSalesPersonDistrictRelation(Guid id, Guid districtId, bool isPrimary, bool isSecondary)
        {
            // Nothing happens
        }

        public void RemoveSalesPersonDistrictRelation(Guid id, Guid districtId)
        {
           // Nothing happens
        }

        public SalesPerson GetSalesPerson(Guid id)
        {
            return new SalesPerson { Id = Guid.Empty, FirstName = "Test First Name", LastName = "Test Last Name", Initials = "TT" };
        }
    }

    public class TestStoreRepository : IStoreRepository
    {
        public List<Store> GetStoresByDistrictId(Guid id)
        {
            return new List<Store> {new Store {Address = "Test Address", Id = Guid.Empty, Name = "Test Name"} };
        }
    }

    #endregion

    #region Service

    public class TestDistrictsService : IDistrictsService
    {
        public DistrictResponse Get(Guid districtId)
        {
            return new DistrictResponse(districtId, "Test District",
                new SalesPerson { Id = districtId, FirstName = "Test First Name", LastName = "Test Last Name", Initials = "TT" },
                new List<SalesPerson>(),
                new List<Store>());
        }


        public DistrictListResponse GetAll()
        {
            return new DistrictListResponse(new List<District> { new District { Id = Guid.Empty, Name = "Test District" } });
        }
    }

    public class TestSalesPersonsService : ISalesPersonsService
    {
        public SalesPersonAddUpdateResponse AddUpdate(Guid salesPersonId, Guid districtId, bool isPrimary, bool isSecondary)
        {
            return new SalesPersonAddUpdateResponse(salesPersonId, districtId, isPrimary, isSecondary);
        }

        public SalesPersonListResponse GetAll()
        {
            return new SalesPersonListResponse(new List<SalesPerson>
                { new SalesPerson {Id = Guid.Empty, FirstName = "Test First Name", LastName = "Test Last Name", Initials = "TT"} });
        }
    }

    #endregion
}
