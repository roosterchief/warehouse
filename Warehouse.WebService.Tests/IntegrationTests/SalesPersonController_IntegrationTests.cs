﻿using System.Net;
using System.Net.Http;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Warehouse.WebService.Tests.IntegrationTests
{
    [TestClass]
    public class SalesPersonController_IntegrationTests : IntegrationTestsBase
    {

        [TestMethod]
        public void GET_salespersons_WhenEverythingIsOk_ReturnsExpectedResult()
        {
            // act
            var response = Server.HttpClient.GetAsync("api/salespersons").Result;
            var result = response.Content.ReadAsStringAsync().Result;

            // assert
            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK);
            Assert.IsTrue(result == "{\"salesPersons\":[{\"id\":\"00000000-0000-0000-0000-000000000000\",\"firstName\":\"Test First Name\",\"lastName\":\"Test Last Name\",\"initials\":\"TT\"}]}");
        }

        [TestMethod]
        public void GET_salespersons_WhenWrongUrl_ReturnsNotFound()
        {
            // act
            var response = Server.HttpClient.GetAsync("api/salespersons2").Result;
            var result = response.Content.ReadAsStringAsync().Result;

            // assert
            Assert.IsTrue(response.StatusCode == HttpStatusCode.NotFound);
            Assert.IsTrue(result == "");
        }

        [TestMethod]
        public void PUT_salespersons_WhenEverythingIsOk_ReturnsExpectedResult()
        {
            // act
            var stringContent = new StringContent("{\"id\":\"00000000-0000-0000-0000-000000000000\",\"districtId\":\"00000000-0000-0000-0000-000000000000\",\"isPrimary\":true,\"isSecondary\":true}", Encoding.UTF8, "application/json");
            var response = Server.HttpClient.PutAsync("api/salespersons/00000000-0000-0000-0000-000000000000", stringContent).Result;
            var result = response.Content.ReadAsStringAsync().Result;

            // assert
            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK);
            Assert.IsTrue(result == "{\"id\":\"00000000-0000-0000-0000-000000000000\",\"districtId\":\"00000000-0000-0000-0000-000000000000\",\"isPrimary\":true,\"isSecondary\":true}");
        }

        [TestMethod]
        public void PUT_salespersons_WhenWrongUrl1_ReturnsNotFound()
        {
            // act
            var stringContent = new StringContent("{\"id\":\"00000000-0000-0000-0000-000000000000\",\"districtId\":\"00000000-0000-0000-0000-000000000000\",\"isPrimary\":true,\"isSecondary\":true}", Encoding.UTF8, "application/json");
            var response = Server.HttpClient.PutAsync("api/salespersons2/00000000-0000-0000-0000-000000000000", stringContent).Result;
            var result = response.Content.ReadAsStringAsync().Result;

            // assert
            Assert.IsTrue(response.StatusCode == HttpStatusCode.NotFound);
            Assert.IsTrue(result == "");
        }

        [TestMethod]
        public void PUT_salespersons_WhenWrongUrl2_ReturnsNotFound()
        {
            // act
            var stringContent = new StringContent("{\"id\":\"00000000-0000-0000-0000-000000000000\",\"districtId\":\"00000000-0000-0000-0000-000000000000\",\"isPrimary\":true,\"isSecondary\":true}", Encoding.UTF8, "application/json");
            var response = Server.HttpClient.PutAsync("api/salespersons/00000000-0000-0000-0000", stringContent).Result;
            var result = response.Content.ReadAsStringAsync().Result;

            // assert
            Assert.IsTrue(response.StatusCode == HttpStatusCode.NotFound);
            Assert.IsTrue(result == "");
        }

        [TestMethod]
        public void PUT_salespersons_WhenMalformedJsonMissingOpeningBracket_ReturnsNotFound()
        {
            // act
            var stringContent = new StringContent("\"id\":\"00000000-0000-0000-0000-000000000000\",\"districtId\":\"00000000-0000-0000-0000-000000000000\",\"isPrimary\":true,\"isSecondary\":true}", Encoding.UTF8, "application/json");
            var response = Server.HttpClient.PutAsync("api/salespersons/00000000-0000-0000-0000-000000000000", stringContent).Result;
            var result = response.Content.ReadAsStringAsync().Result;

            // assert
            Assert.IsTrue(response.StatusCode == HttpStatusCode.BadRequest);
            Assert.IsTrue(result == "");
        }

        [TestMethod]
        public void PUT_salespersons_WhenUpplerCaseUsed_ReturnsExpectedResults()
        {
            // act
            var stringContent = new StringContent("{\"ID\":\"00000000-0000-0000-0000-000000000000\",\"DISTRICTID\":\"00000000-0000-0000-0000-000000000000\",\"isPrimary\":true,\"isSecondary\":true}", Encoding.UTF8, "application/json");
            var response = Server.HttpClient.PutAsync("api/salespersons/00000000-0000-0000-0000-000000000000", stringContent).Result;
            var result = response.Content.ReadAsStringAsync().Result;

            // assert
            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK);
            Assert.IsTrue(result == "{\"id\":\"00000000-0000-0000-0000-000000000000\",\"districtId\":\"00000000-0000-0000-0000-000000000000\",\"isPrimary\":true,\"isSecondary\":true}");
        }
    }
}
