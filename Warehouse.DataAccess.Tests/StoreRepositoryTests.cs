﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Warehouse.DataAccess.Entities;

namespace Warehouse.DataAccess.Tests
{
    [TestClass]
    public class StoreRepositoryTests : RepositryTestsBase
    {
        #region GetStoresByDistrictId

        [TestMethod]
        public void GetStoresByDistrictId_WhenKnownDistrictHasStores_ReturnsStoreList()
        {
            // arrange
            var districtRepo = new DistrictRepository();
            var district = districtRepo.GetAllDistricts().FirstOrDefault();
            if (district != null)
            {
                var storeRepo = new StoreRepository();

                // act
                var stores = storeRepo.GetStoresByDistrictId(district.Id);

                // assert
                Assert.IsTrue(stores != null);
                Assert.IsInstanceOfType(stores, typeof(List<Store>));
                Assert.IsTrue(stores.Count > 0);
                AssertStoreHasData(stores.FirstOrDefault());
            }
            else
            {
                Assert.Inconclusive("No district data found in DB.");
            }
        }

        [TestMethod]
        public void GetStoresByDistrictId_WhenUnknownDistrict_ReturnsEmptyStoreList()
        {
            // arrange
            var storeRepo = new StoreRepository();

            // act
            var stores = storeRepo.GetStoresByDistrictId(Guid.Empty);

            // assert
            Assert.IsTrue(stores != null);
            Assert.IsInstanceOfType(stores, typeof(List<Store>));
            Assert.IsTrue(stores.Count == 0);
        }

        [TestMethod]
        public void GetStoresByDistrictId_WhenNoStores_ReturnsEmptyStoreList()
        {
            // arrange
            ExecuteScript(@"..\..\..\Database\cleanup_stores.sql");
            var districtRepo = new DistrictRepository();
            var district = districtRepo.GetAllDistricts().FirstOrDefault();
            if (district != null)
            {
                var storeRepo = new StoreRepository();

                // act
                var stores = storeRepo.GetStoresByDistrictId(district.Id);

                // assert
                Assert.IsTrue(stores != null);
                Assert.IsInstanceOfType(stores, typeof(List<Store>));
                Assert.IsTrue(stores.Count == 0);
            }
            else
            {
                Assert.Inconclusive("No district data found in DB.");
            }
        }

        #endregion

        private static void AssertStoreHasData(Store store)
        {
            Assert.IsTrue(store != null);
            Assert.IsTrue(store.Id != Guid.Empty);
            Assert.IsTrue(!string.IsNullOrWhiteSpace(store.Name));
            Assert.IsTrue(!string.IsNullOrWhiteSpace(store.Address));
        }
    }
}
