﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Warehouse.DataAccess.Entities;

namespace Warehouse.DataAccess.Tests
{
    [TestClass]
    public class DistrictRepositoryTests : RepositryTestsBase
    {
        #region GetAllDistricts

        [TestMethod]
        public void GetAllDistricts_WhenDatabaseHasDistricts_ReturnsDistrictList()
        {
            // arrange
            var repository = new DistrictRepository();

            // act
            var districts = repository.GetAllDistricts();

            // assert
            Assert.IsTrue(districts != null);
            Assert.IsInstanceOfType(districts, typeof(List<District>));
            Assert.IsTrue(districts.Count > 0);

        }

        [TestMethod]
        public void GetAllDistricts_WhenDatabaseDoesNotHaveDistricts_ReturnsEmptyDistrictList()
        {
            // arrange
            CleanupDatabase(); // Just for this unit test!
            var repository = new DistrictRepository();

            // act
            var districts = repository.GetAllDistricts();

            // assert
            Assert.IsTrue(districts != null);
            Assert.IsInstanceOfType(districts, typeof(List<District>));
            Assert.IsTrue(districts.Count == 0);

        }
        
        #endregion

        #region GetDistrict

        [TestMethod]
        public void GetDistrict_WhenParameterIsKnownDistrictId_ReturnsDistrictObject()
        {
            // arrange
            var repository = new DistrictRepository();
            var district = repository.GetAllDistricts().FirstOrDefault();
            if (district != null)
            {
                // act
                var districtTarget = repository.GetDistrict(district.Id);

                // assert
                AssertDistrictHasData(districtTarget);
            }
            else
            {
                Assert.Inconclusive("No districts data found in DB.");
            }  
                    
        }

        [TestMethod]
        public void GetDistrict_WhenParameterIsUnknownDistrictId_ReturnsNull()
        {
            // arrange
            var repository = new DistrictRepository();
            
            // act
            var district = repository.GetDistrict(Guid.Empty);

            // assert
            Assert.IsTrue(district == null);
        }
       
        #endregion

        private static void AssertDistrictHasData(District district)
        {
            Assert.IsTrue(district != null);
            Assert.IsInstanceOfType(district, typeof(District));
            Assert.IsTrue(district.Id != Guid.Empty);
            Assert.IsTrue(!string.IsNullOrWhiteSpace(district.Name));
        }
    }
}
