﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Warehouse.DataAccess.Tests
{
    [TestClass]
    public abstract class RepositryTestsBase
    {
        [TestInitialize]
        public void SeedDatabase()
        {
            ExecuteScript(@"..\..\..\Database\seed_database.sql");
        }

        [TestCleanup]
        public void CleanupDatabase()
        {
            ExecuteScript(@"..\..\..\Database\cleanup_database.sql");
        }

        protected void ExecuteScript(string pathToScript)
        {
            string sqlConnectionString = ConfigurationManager.ConnectionStrings["warehouseDB"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sqlConnectionString))
            {
                Server server = new Server(new ServerConnection(conn));

                if (!File.Exists(pathToScript))
                {
                    Console.WriteLine($"The following path '{pathToScript}' does not exist!");
                    string currentDirectory = Directory.GetCurrentDirectory();
                    Console.WriteLine($"The current path is: '{currentDirectory}'.");
                    Console.WriteLine("Fix the path!");

                    Assert.Inconclusive();
                }

                string script = File.ReadAllText(pathToScript);
                server.ConnectionContext.ExecuteNonQuery(script);
            }
        }
    }
}
