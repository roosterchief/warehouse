﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Warehouse.DataAccess.Entities;

namespace Warehouse.DataAccess.Tests
{
    [TestClass]
    public class SalesPersonRepositoryTests : RepositryTestsBase
    {
        #region GetPrimarySalesPersonByDistrictId

        [TestMethod]
        public void GetPrimarySalesPersonByDistrictId_WhenKnownDistrictIdHasPrimarySalesPerson_ReturnsSalesPersonObject()
        {
            // arrange
            var districtRepo = new DistrictRepository();
            var district = districtRepo.GetAllDistricts().FirstOrDefault();
            if (district != null)
            {
                var salesPersonRepo = new SalesPersonRepository();

                // act
                var salesPerson = salesPersonRepo.GetPrimarySalesPersonByDistrictId(district.Id);

                // assert
                AssertSalesPersonHasData(salesPerson);
            }
            else
            {
                Assert.Inconclusive("No district data found in DB.");
            }
        }

        [TestMethod]
        public void GetPrimarySalesPersonByDistrictId_WhenUnknownDistrictId_ReturnsNull()
        {
            // arrange
            var salesPersonRepo = new SalesPersonRepository();

            // act
            var salesPerson = salesPersonRepo.GetPrimarySalesPersonByDistrictId(Guid.Empty);

            // assert
            Assert.IsTrue(salesPerson == null);
        }

        [TestMethod]
        public void GetPrimarySalesPersonByDistrictId_WhenKnownDistrictIdHasNoPrimarySalesPerson_ReturnsNull()
        {
            // arrange
            ExecuteScript(@"..\..\..\Database\cleanup_primary_salespersons.sql");
            var districtRepo = new DistrictRepository();
            var district = districtRepo.GetAllDistricts().FirstOrDefault();
            if (district != null)
            {
                var salesPersonRepo = new SalesPersonRepository();
                
                // act
                var salesPerson = salesPersonRepo.GetPrimarySalesPersonByDistrictId(district.Id);

                // assert
                Assert.IsTrue(salesPerson == null);
            }
            else
            {
                Assert.Inconclusive("No district data found in DB.");
            }
        }

        #endregion

        #region GetSecondarySalesPersonsByDistrictId

        [TestMethod]
        public void GetSecondarySalesPersonsByDistrictId_WhenKnownDistrictIdHasSecondarySalesPersons_ReturnsSalesPersonList()
        {
            // arrange
            var districtRepo = new DistrictRepository();
            var district = districtRepo.GetAllDistricts().FirstOrDefault();
            if (district != null)
            {
                var salesPersonRepo = new SalesPersonRepository();

                // act
                var salesPersons = salesPersonRepo.GetSecondarySalesPersonsByDistrictId(district.Id);

                // assert
                Assert.IsTrue(salesPersons != null);
                Assert.IsInstanceOfType(salesPersons, typeof(List<SalesPerson>));
                Assert.IsTrue(salesPersons.Count > 0);
                AssertSalesPersonHasData(salesPersons.FirstOrDefault());
            }
            else
            {
                Assert.Inconclusive("No district data found in DB.");
            }
        }

        [TestMethod]
        public void GetSecondarySalesPersonsByDistrictId_WhenUnknownDistrictId_ReturnsEmptySalesPersonList()
        {
            // arrange
            var salesPersonRepo = new SalesPersonRepository();

            // act
            var salesPersons = salesPersonRepo.GetSecondarySalesPersonsByDistrictId(Guid.Empty);

            // assert
            Assert.IsTrue(salesPersons != null);
            Assert.IsInstanceOfType(salesPersons, typeof(List<SalesPerson>));
            Assert.IsTrue(salesPersons.Count == 0);
        }

        [TestMethod]
        public void GetSecondarySalesPersonsByDistrictId_WhenKnownDistrictIdHasNotSecondarySalesPersons_ReturnsEmptySalesPersonsList()
        {
            // arrange
            ExecuteScript(@"..\..\..\Database\cleanup_secondary_salespersons.sql");
            var districtRepo = new DistrictRepository();
            var district = districtRepo.GetAllDistricts().FirstOrDefault();
            if (district != null)
            {
                var salesPersonRepo = new SalesPersonRepository();

                // act
                var salesPersons = salesPersonRepo.GetSecondarySalesPersonsByDistrictId(district.Id);

                // assert
                Assert.IsTrue(salesPersons != null);
                Assert.IsInstanceOfType(salesPersons, typeof(List<SalesPerson>));
                Assert.IsTrue(salesPersons.Count == 0);
            }
            else
            {
                Assert.Inconclusive("No district data found in DB.");
            }
        }

        #endregion

        #region AddUpdateSalesPersonDistrictRelation

        [TestMethod]
        public void AddUpdateSalesPersonDistrictRelation_WhenKnownDistsrictIdAndSalesPersonId_Executes()
        {
            // arrange
            var districtRepo = new DistrictRepository();
            var district = districtRepo.GetAllDistricts().First(x => x.Id == Guid.Parse("00000000-0000-0000-0000-000000000001")); // We know that from 'seed_database.sql' script
            if (district != null)
            {
                var salesPersonRepo = new SalesPersonRepository();
                var primarySalesPerson = salesPersonRepo.GetPrimarySalesPersonByDistrictId(district.Id);
                if (primarySalesPerson != null)
                {
                    // act
                    salesPersonRepo.AddUpdateSalesPersonDistrictRelation(primarySalesPerson.Id, district.Id, true, true);

                    // assert
                    var dbPrimarySalesPerson = salesPersonRepo.GetPrimarySalesPersonByDistrictId(district.Id);
                    var dbSecondarySalesPerson = salesPersonRepo.GetSecondarySalesPersonsByDistrictId(Guid.Parse("00000000-0000-0000-0000-000000000001")).First(x => x.Id == dbPrimarySalesPerson.Id);
                    AssertSalesPersonHasData(dbSecondarySalesPerson);
                }
                else
                {
                    Assert.Inconclusive($"No primary salesperson data found for district [{district.Name}] in DB.");
                }
            }
            else
            {
                Assert.Inconclusive("No district data found in DB.");
            }
        }

        [TestMethod]
        [ExpectedException(typeof(SqlException))]
        public void AddUpdateSalesPersonDistrictRelation_WhenUnknownDistsrictIdAndSalesPersonId_ThrowsSqlException()
        {
            // arrange
            var salesPersonRepo = new SalesPersonRepository();

            // act
            salesPersonRepo.AddUpdateSalesPersonDistrictRelation(Guid.Empty, Guid.Empty, true, true);
	        
        }

        [TestMethod]
        [ExpectedException(typeof(SqlException))]
        public void AddUpdateSalesPersonDistrictRelation_WhenThereIsAlreadyPrimarySalesPerson_ThrowsSqlException_()
        {
            // arrange
            var districtRepo = new DistrictRepository();
            var district = districtRepo.GetAllDistricts().First(x => x.Id == Guid.Parse("00000000-0000-0000-0000-000000000001")); // We know that from 'seed_database.sql' script
            if (district != null)
            {
                var salesPersonRepo = new SalesPersonRepository();
                var primarySalesPerson = salesPersonRepo.GetPrimarySalesPersonByDistrictId(district.Id);
                var fakePrimarySalesPerson = salesPersonRepo.GetAll().First(x => x.Id != primarySalesPerson.Id);
                if (fakePrimarySalesPerson != null)
                {
                    // act
                    salesPersonRepo.AddUpdateSalesPersonDistrictRelation(fakePrimarySalesPerson.Id, district.Id, true, true);
                }
                else
                {
                    Assert.Inconclusive("No other primary salespersons in DB.");
                }
            }
            else
            {
                Assert.Inconclusive("No district data found in DB.");
            }
        }

        [TestMethod]
        public void AddUpdateSalesPersonDistrictRelation_WhenOnlyAddUpdateSecondarySalesPerson_ExecutesAlways()
        {
            // arrange
            var districtRepo = new DistrictRepository();
            var district = districtRepo.GetAllDistricts().First(x => x.Id == Guid.Parse("00000000-0000-0000-0000-000000000001")); // We know that from 'seed_database.sql' script
            if (district != null)
            {
                var salesPersonRepo = new SalesPersonRepository();
                var primarySalesPerson = salesPersonRepo.GetPrimarySalesPersonByDistrictId(district.Id);
                var secondarySalesPersons = salesPersonRepo.GetSecondarySalesPersonsByDistrictId(district.Id);
                List<Guid> listIds = secondarySalesPersons.Select(x => x.Id).ToList();

                var newSecondarySalesPerson = salesPersonRepo.GetAll().First(x => x.Id != primarySalesPerson.Id && !listIds.Contains(x.Id));
                if (newSecondarySalesPerson != null)
                {
                    // act
                    salesPersonRepo.AddUpdateSalesPersonDistrictRelation(newSecondarySalesPerson.Id, district.Id, false, true);
                }
                else
                {
                    Assert.Inconclusive("No other salespersons in DB.");
                }
            }
            else
            {
                Assert.Inconclusive("No district data found in DB.");
            }
        }

        #endregion

        #region RemoveSalesPersonDistrictRelation

        [TestMethod]
        public void RemoveSalesPersonDistrictRelation_WhenKnownDistsrictIdAndSalesPersonId_Executes()
        {
            // arrange
            var districtRepo = new DistrictRepository();
            var district = districtRepo.GetAllDistricts().FirstOrDefault();
            if (district != null)
            {
                var salesPersonRepo = new SalesPersonRepository();
                var salesPersons = salesPersonRepo.GetSecondarySalesPersonsByDistrictId(district.Id);
                var secondarySalesPerson = salesPersons.FirstOrDefault();
                if (secondarySalesPerson != null)
                {
                    // act + assert
                    salesPersonRepo.RemoveSalesPersonDistrictRelation(secondarySalesPerson.Id, district.Id);

                    // ...And restore the relation again. We need to keep the data unmodified.
                    salesPersonRepo.AddUpdateSalesPersonDistrictRelation(secondarySalesPerson.Id, district.Id, true, true);
                }
                else
                {
                    Assert.Inconclusive($"No secondary salesperson data found for district [{district.Name}] in DB.");
                }
            }
            else
            {
                Assert.Inconclusive("No district data found in DB.");
            }
        }

        [TestMethod]
        public void RemoveSalesPersonDistrictRelation_WhenUnknownDistsrictIdAndSalesPersonId_Executes()
        {
            // arrange
            var salesPersonRepo = new SalesPersonRepository();

            // act + assert
            salesPersonRepo.RemoveSalesPersonDistrictRelation(Guid.Empty, Guid.Empty);
        }

        #endregion

        #region GetAll

        [TestMethod]
        public void GetAllSalesPersons_WhenDatabaseHasSalesPersons_ReturnsSalesPersonList()
        {
            // arrange
            var repository = new SalesPersonRepository();

            // act
            var salesPersons = repository.GetAll();

            // assert
            Assert.IsTrue(salesPersons != null);
            Assert.IsInstanceOfType(salesPersons, typeof(List<SalesPerson>));
            Assert.IsTrue(salesPersons.Count > 0);
        }

        [TestMethod]
        public void GetAllSalesPersons_WhenDatabaseDoesNotHaveSalesPersons_ReturnsEmptySalesPersonList()
        {
            // arrange
            ExecuteScript(@"..\..\..\Database\cleanup_salespersons.sql");
            var repository = new SalesPersonRepository();

            // act
            var salesPersons = repository.GetAll();

            // assert
            Assert.IsTrue(salesPersons != null);
            Assert.IsInstanceOfType(salesPersons, typeof(List<SalesPerson>));
            Assert.IsTrue(salesPersons.Count == 0);
        }

        #endregion

        #region Get

        [TestMethod]
        public void GetSalesPerson_WhenKnownSalesPersonId_ReturnsSalesPerson()
        {
            // arrange
            var repository = new SalesPersonRepository();
            var salesPerson = repository.GetAll().FirstOrDefault();
            if (salesPerson != null)
            {
                var salesPersonRepo = new SalesPersonRepository();

                // act
                var salesPersonTarget = salesPersonRepo.GetSalesPerson(salesPerson.Id);

                // assert
                AssertSalesPersonHasData(salesPersonTarget);
            }
            else
            {
                Assert.Inconclusive("No sales persons data found in DB.");
            }
        }

        [TestMethod]
        public void GetSalesPerson_WhenUnknownSalesPersonId_ReturnsNull()
        {
            // arrange
            var salesPersonRepo = new SalesPersonRepository();

            // act
            var salesPerson = salesPersonRepo.GetSalesPerson(Guid.Empty);

            // assert
            Assert.IsTrue(salesPerson == null);
        }

        #endregion

        private static void AssertSalesPersonHasData(SalesPerson salesPerson)
        {
            Assert.IsTrue(salesPerson != null);
            Assert.IsInstanceOfType(salesPerson, typeof(SalesPerson));
            Assert.IsTrue(salesPerson.Id != Guid.Empty);
            Assert.IsTrue(!string.IsNullOrWhiteSpace(salesPerson.FirstName));
            Assert.IsTrue(!string.IsNullOrWhiteSpace(salesPerson.LastName));
            Assert.IsTrue(!string.IsNullOrWhiteSpace(salesPerson.Initials));
        }
    }
}
